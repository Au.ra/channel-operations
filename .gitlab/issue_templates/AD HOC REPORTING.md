# AD HOC REPORTING

### Please read the report-specific details on the checklist  below, which provide additional relevant information that may be helpful for ChOps.

### Report/Dashboard Type:


- [ ] **Channel Opportunities** 

- [ ] **Partner Accounts** _[ Text ]_

- [ ] **Partner Deal Registrations** _[ Text ]_

- [ ] **Partner Contacts** 


### Does this comply with access requirements?

- [ ] **Yes** 

- [ ] **No** 

- [ ] **Not Sure** 

### Report Description:
<!--
Please provide a description of the report you are trying to obtain below
-->
- 

### Required Fields:
<!--
Please pprovide any fields that you require to be included in the report below
-->
- 


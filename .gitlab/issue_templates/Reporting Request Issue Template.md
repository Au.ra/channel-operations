# AD HOC REPORTING

### Please read the report-specific details on the checklist  below, which provide additional relevant information that may be helpful for ChOps.

### Report/Dashboard Type:
<!--
Please select report requirements below


- [ ] **Channel Opportunities** 

- [ ] **Partner Accounts** 

- [ ] **Partner Deal Registrations** 

- [ ] **Partner Contacts** 


### Compliance:

- [ ] **Does this comply with access requirements?** _[ Text ]_

### Report Description:
<!--
Please provide a description of the report you are trying to obtain below
-->
- 

### Required Fields:
<!--
Please pprovide any fields that you require to be included in the report below
-->
- 



- 

/label ~"Channel Ops: Data and Reporting” ~"Channel Ops”
